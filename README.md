# WS2021 Workbench for German NER Tools
The following Streamlit's GitLab badge allows you to have a look at the workbench and try some tasks. Note that
- Since my account by Streamlit is free, I can only have 800M [Streamlit cloud space](https://discuss.streamlit.io/t/error-deploying-app-error-checking-streamlit-healthz-connection-refused/9063) when I want to deploy an APP. 
- In order to let you **<ins>play with this APP without installing anything</ins>** and to save memory space at the same time, I have **<ins>additionally pushed a simplified version</ins>** of this project in [Github](https://github.com/heyjing/Workbench_German_NER), where there are only two models to choose from (spacy small and middle). 
- Note that the deployed version may crash after being used several times because of lack of space.

[![Streamlit App](https://static.streamlit.io/badges/streamlit_badge_black_white.svg)](https://share.streamlit.io/heyjing/workbench_german_ner/main/ner_app.py)

Advanced practical by Jing Fan, supervised by Prof. Dr. Michael Gertz

This advanced practical designs and implements a workbench for comparing 
NER tools for German texts and recommends the best tools which should be used in different situations.

## Table of contents
- 1 Introduction
    - 1.1 Motivation and objectives
    - 1.2 Selected models 
- 2 Implementation and features of workbench
- 3 Evaluation
    - 3.1 Evaluation using domain-general data
    - 3.2 Evaluation using domain-specific data
        - 3.2.1 Political datasets
        - 3.2.2 laws
        - 3.2.3 news corpora
- 4 Summary
- 5 Project Setup
- 6 About the author
- 7 Literature


## 1 Introduction
### 1.1 Motivation and objectives
NER is important for many fields. In the recent years, many NER models are trained, such as Spacy, NLTK, Flair, Bert, AllenNLP and so on. These models differ in the processing method they rely upon, the dataset they were trained, the entity types they can detect, the nature of the text they can handle, the processing speed and the natural languages they support. This makes it difficult for a user to select an appropriate NER tool for a specific task [[1]](#7-literature).

NER for German is especially challenging, since the capitalization feature is less useful than in other Western languages such as English or Spanish [[2]](#7-literature).

Therefore, the main task in this practical is to design and implement a workbench that can compare the performance of different NER tools for German texts. Besides, the practical should also evaluate the performance of selected models on some domain-specific texts and answer the question "Which model should I take for which task?".

### 1.2 Selected models
This workbench consists of nine models that belong to four main categories: 
- Spacy 
    - [Spacy small](https://spacy.io/models/de)
    - [Spacy middle](https://spacy.io/models/de)
    - [Spacy large](https://spacy.io/models/de)
- [NLTK](https://www.nltk.org/install.html)
- Bert 
    - [fhswf/bert_de_ner](https://huggingface.co/fhswf/bert_de_ner)
    - [Davlan/bert-base-multilingual-cased-ner-hrl](https://huggingface.co/Davlan/bert-base-multilingual-cased-ner-hrl)
- Flair 
    - [flair/ner-german](https://huggingface.co/flair/ner-german)
    - [flair/ner-german-large](https://huggingface.co/flair/ner-german-large)
    - [flair/ner-multi](https://huggingface.co/flair/ner-multi)

It is very easy to add other models in this Workbench.

## 2 Implementation and features of workbench
This workbench consists of a front end and a back end. The front end was created in Python using [Streamlit](https://docs.streamlit.io/library/api-reference). 

![alt text](Workbench_architecture_and_features/Architecture.png "Architecture")

In the following, an introduction to the features implemented in the front end will be given.


- **① Data upload field**: 
Users can upload .csv ground truth data or .txt data file (implemented through <code>st.sidebar.file_uploader</code>) or input texts (implemented through <code>st.sidebar.text_area</code>) in the sidebar. After the file is uploaded successfully, the user can see the button of "Begin Processing". Before pressing the button, the user needs to select at least one model, and the number of models selected should not exceed the maximum number. If the model field is empty or the the number of models selected exceeds the maximum number allowed, the user will see a warning.

- **② Model selection field**:
This feature is implemented through a multiselect widget (<code>st.multiselect</code>). The multiselect widget starts as empty. There are nine models that can be selected. User can select up to three models by default. 

- **③ Setting of maximal number of models allowed**:
This feature is implemented through a single select widget (<code>st.selectbox</code>). Users can set the maximal models allowed for select themselves.

- **④ Statistic**:
This field shows the statistic. When it is .csv ground truth data, the precision, recall and f_value of the total data and of each entity type will be showed. When it is .txt data, the number of entities of each type found by a model will be displayed.


- **⑤ Visualization**:
This field visualizes the results of selected models. It allows people to visually see the results of each model. The visualization used [displaCy](https://spacy.io/usage/visualizers) to manually render data. The generated html string will be rendered using Streamlit Markdown (<code>st.markdown</code>). 

- **⑥ Setting of entity types**:
This feature is implemented through a multiselect widget (<code>st.multiselect</code>). By default, three entity types (PER, LOC and ORG) are selected. Therefore, users can see all these three types in the visualization field. Users can change the entity types which will be shown in the visualization field themselves.

- **⑦ Interaktive figures**:
This figure visualizes the statistic results of all selected models in one plot. It is generated using [plotly](https://plotly.com/python/plotly-express/#overview). After the plot is generated, it is shown in the front end using <code>st.plotly_chart</code>.

![alt text](Workbench_architecture_and_features/interaktive_plot_example.gif "Workbench")

- **⑧ Progress bar**:
Sometimes a text a long and it needs time for thr processing. The progress bar can let the user know where the program is currently running and how long it will still take. It is implemeted through [stqdm](https://github.com/Wirg/stqdm).

<img src="Workbench_architecture_and_features/progress_bar_under_model.png" alt="progress_bar" width="300"/><br>
<img src="Workbench_architecture_and_features/progress_bar_demo.gif" alt="progress_bar" width="600"/>

This is a screenshot of the workbench. The numbers in the screenshot correspond to the numbers of the features explained above.

![alt text](Workbench_architecture_and_features/Workbench_screenshot_1.png "Workbench")

![alt text](Workbench_architecture_and_features/Workbench_screenshot_2.png "Workbench")

## 3 Evaluation
### 3.1 Evaluation using domain-general data
The [GermEval 2014 NER Shared Task (Development Set)](https://sites.google.com/site/germeval2014ner/data) was used as an example of domain-general data. We used the metric **f_value** to compare the nine models. We can easily observe five phenomena. 
- First, of all the models, the NLTK model performed the worst. The reason is that NLTK's NER is not trained for German.

![alt text](evaluation_results/GermEval2014_f_value_nine_models_with_nltk_.png "GermEval2014_f_value_nine_models(with_nltk)")

- Second, all models generally perform best on the PER entity type (PER > LOC > ORG). 
- Third, the lines in the chart do not cross too much. This means that a better model generally performs better on all entity types.
- Fourth, the total f_values of BERT and Flair models are very similar (about 0.75). The difference between Spacy model and other models is large.

![alt text](evaluation_results/GermEval2014_f_value_eight_models.png "GermEval2014_f_value_eight_models_without_spacy")

- Fifth, it can be observed that performance and processing time are inversely proportional. The better the performance, the longer the processing time.

![alt text](evaluation_results/ranking_of_models_GermEval2014.png "ranking_of_models_GermEval2014")



### 3.2 Evaluation using domain-specific data
#### 3.2.1 Political datasets

The political dataset used in the evaluation can be obtained [here](https://www.thomas-zastrow.de/nlp/). We observe the following three phenomena: 
- Spacy performs poorly compared to other models, it recognized fewer types and was less accurate at identifying types. This is consistent with what we observed in the GermEval 2014 dataset.
- Among the three spacy models, the performance of small spacy model is the closest to the performances of other more accurate models. 
- The difference of BERT and Flair models is not very large.

![alt text](evaluation_results/political_dataset_eight_models.png "political_dataset_eight_models")


#### 3.2.2 laws

The Grundgesetz can be obtained [here](https://github.com/levinalex/deutsche_verfassungen/blob/master/grundgesetz/grundgesetz.txt). The legal domain is a special domain and legal texts are characterized by the ORG entity type being the majority of all entity types [[3]](#7-literature). The following results of different models show:
- Spacy performs poorly compared to other models. It recognizes many entities that are not of LOC type as LOC.
- The results of the remaining five models show that ORG entity type is the majority of all entity types in the Grundgesetz, which are in consistent with the distribution of different entity types in the dataset in [[3]](#7-literature).
- The fhswf/bert_de_ner model identified the most ORG entity types.

![alt text](evaluation_results/Grundgesetz_eight_models.png "Grundgesetz_eight_models")

![alt text](evaluation_results/Grundgesetz_five_models_without_spacy.png "Grundgesetz_eight_models_without_spacy")


#### 3.2.3 news corpora
The news corpora 2021 dataset can be obtained [here](https://wortschatz.uni-leipzig.de/de/download/German#deu_news_2020) (Category **News**, 2021).
Some similar phenomena were also observed through this dataset:
- Spacy performs poorly compared to other models, especially the spacy large model.
- Among the three spacy models, the performance of small spacy model is the closest to the performances of other more accurate models. 
- The difference between BERT and Flair models is not large. (The number of the three entity types is above 1900, and the difference between differnt BERT and Flair models is within 300.)

![alt text](evaluation_results/news_corpora_2021_eight_models.png "news_corpora_2021_eight_models")

![alt text](evaluation_results/news_corpora_2021_five_models_without_spacy.png "news_corpora_2021_five_models_without_spacy")

## 4 Summary

Through the evaluation using domain-general and domain-specific data, we have observed some common phenomena：
- All models generally perform best on the PER entity type.
- The performances of BERT and Flair models are similar. The difference between Spacy model and other models is large.
- Performance and processing time are basically inversely proportional. But for the five BERT and Flair models that perform similarly, the two BERT model need significantly less time than Flair, and flair/ner-german-large takes significantly less time than the flair/ner-german and flair/ner-multi.
- Among the three spacy models, the performance of small spacy model is the closest to the performances of other more accurate models. 

Based on the evaluation using domain-general and domain-specific data, we give recommendations on when to use which model in the table below.

| Text types | Time is the most important measure | Performance is the most important measure | Both time and performance matter | 
| ------ | ------ | ------ | ------ |
| Domain-general data | Spacy Small | flair/ner-german-large | Either of the two bert models |
| Political text | Spacy Small | flair/ner-german-large or flair/ner-multi | Either of the two bert models |
| Legal text | Spacy Small | fhswf/bert_de_ner | fhswf/bert_de_ner |
| News corpora | Spacy Small | flair/ner-multi | Either of the two bert models |

## 5 Project Setup

- Create a virtual environment with conda <br>
```
conda create --name Workbench_German_NER python=3.8
conda activate Workbench_German_NER
```

- Clone this gitlab repository <br>
```
git colone https://git-dbs.ifi.uni-heidelberg.de/practicals/german_ner_jing_fan.git
```

- Install required packages <br>
```
pip install -r requirements.txt
```

- Following models need to be installed before using this APP: <br>
    
```
python -m spacy download de_core_news_sm
python -m spacy download de_core_news_md
python -m spacy download de_core_news_lg

pip install nltk
```

- Other NER models can be installed automatically on first use.

- To launch the APP locally, in the terminal, you can run the following command: <code>streamlit run streamlit_sharing.py</code>. Please make sure that you need to navigate to the directory where the Python script is saved.
```
streamlit run ...YOURPATH/Workbench_German_NER/german_ner_jing_fan/Codes/ner_app.py
```

- In case you see an error after opening this APP, you can clear caches via the UI: click on the ☰ menu, then “Clear cache”, or via the command line:
```
streamlit cache clear
```

## 6 About the author
This project was created by Jing Fan (email: j.fan@stud.uni-heidelberg.de).

## 7 Literature
<a id="1">[1]</a>
Atdağ, S., Labatut, V. (2013). A Comparison of Named Entity Recognition Tools Applied to Biographical Texts. 2nd International Conference on Systems and Computer Science, Villeneuve d'Ascq (FR), 228-233. https://arxiv.org/abs/1308.0661.

<a id="2">[2]</a>
Benikova1, D., Benikova1, C., Reznicek, M. (2014). NoSta-D Named Entity Annotation for German: Guidelines and Dataset. https://www.semanticscholar.org/paper/NoSta-D-Named-Entity-Annotation-for-German%3A-and-Benikova-Biemann/0a1cf8dbb859c13cbdb40788d7e69060155f9d77.

<a id="3">[3]</a>
Leitner, E., Rehm, G., Moreno-Schneider, J. (2020). A Dataset of German Legal Documents for Named Entity Recognition. Proceedings of the 12th Conference on Language Resources and Evaluation (LREC 2020), pages 4478–4485. http://www.lrec-conf.org/proceedings/lrec2020/pdf/2020.lrec-1.551.pdf.



